import { api } from "../utils/api";

export const getAllUsers = async () => {
  let response = await api.get("/users");
  return response.data;
};
