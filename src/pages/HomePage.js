import React from "react";
import CounterComponent from "../components/CounterComponent";

const HomePage = () => {
  return (
    <div className="container">
      <h1>This is the home page</h1>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum corrupti
        tenetur maxime nisi beatae, nulla aspernatur incidunt aliquam vel
        adipisci laborum tempore unde, rerum magni id voluptatibus quibusdam
        praesentium reiciendis.
      </p>

      <CounterComponent />
    </div>
  );
};

export default HomePage;
