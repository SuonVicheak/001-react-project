import React from "react";
import { NavLink } from "react-router-dom";

const NotFoundPage = () => {
  return (
    <div className="container my-5 text-center">
      <img
        className="w-50 img-fluid"
        src="https://cdn3d.iconscout.com/3d/premium/thumb/404-3025721-2526919.png"
        alt="page not found"
      />
      <h3>Page Not Found!!</h3>
      <p>Seems like you have navigated to the wrong page!</p>
      <NavLink to={"/"}>
        <button className="btn btn-warning">Go Back Home </button>
      </NavLink>
    </div>
  );
};

export default NotFoundPage;
