import React, { useEffect, useState } from "react";
import { getAllUsers } from "../services/userService";
import ProfileCard from "../components/ProfileCard";
import Loading from "../components/Loading";

const UsersPage = () => {
  const [users, setUsers] = useState([]);

  //first param : callback function,
  //second param : dependency (when to execute the function)
  useEffect(() => {
    getAllUsers()
      .then((res) => {
        //we successfully get the data back from the request
        setUsers(res);
        console.log(res);
      })
      .catch((err) => console.log("Error fetching users", err));
  }, []);

  //array to store profile card components
  let loadingCards = [];

  //loop to create many loadingCards
  for (let i = 0; i < 8; i++) {
    loadingCards.push(
      <div className="col-lg-3 col-md-4 col-sm-6 mt-4" key={i}>
        <Loading />
      </div>
    );
  }

  return (
    <div className="container">
      <h1>All Users names : </h1>
      {/* {users.map((user, index) => {
        return <h3 key={index}>{user.name}</h3>;
      })}*/}

      <div className="container">
        <div className="row">
          {users.length === 0 && loadingCards}
          {users.map((user, index) => {
            return (
              <div className="col-lg-3 col-md-4 col-sm-6 mt-4" key={index}>
                <ProfileCard user={user} />
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default UsersPage;
