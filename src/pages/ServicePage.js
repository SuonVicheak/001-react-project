import React, { useState } from "react";

//useState
//useRef
//useEffect

const ServicePage = () => {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [role, setRole] = useState("");
  const [selectedFile, setSelectedFile] = useState(null);

  //what we want is an object of user
  const userData = {
    username,
    email,
    password,
    role: role == 1 ? "Customer" : "Admin",
  };

  console.log("User Data is :", userData);

  const handleImageChange = (e) => {
    //console.log("Event : ", e);
    //setSelectedFile(e.target.files[0]);

    let imageUrl = URL.createObjectURL(e.target.files[0]);
    console.log(imageUrl);
    setSelectedFile(imageUrl);
  };

  return (
    <div className="container mx-auto">
      <h1>Register Form</h1>
      <div className="container mt-5 d-flex gap-3 justify-content-center bg-light rounded py-5">
        <div className="container flex-column text-center w-25">
          <img
            className="img-fluid rounded"
            width={"270px"}
            src={
              selectedFile
                ? selectedFile
                : "https://i.pinimg.com/736x/89/90/48/899048ab0cc455154006fdb9676964b3.jpg"
            }
            alt="Image Profile for user"
          />
          <input
            className="form-control mt-2"
            type="file"
            name=""
            id=""
            onChange={handleImageChange}
          />
        </div>

        <div className="w-75">
          <div className="d-flex justify-content-between ">
            <div className="mb-3">
              <label htmlFor="exampleFormControlInput0" className="form-label">
                Username
              </label>
              <input
                type="text"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
                className="form-control"
                id="exampleFormControlInput0"
                placeholder="John Doe"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="exampleFormControlInput1" className="form-label">
                Email address
              </label>
              <input
                type="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                className="form-control"
                id="exampleFormControlInput1"
                placeholder="name@example.com"
              />
            </div>
          </div>
          <div className="mb-3">
            <label htmlFor="exampleFormControlInput2" className="form-label">
              Password
            </label>
            <input
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              className="form-control"
              id="exampleFormControlInput2"
              placeholder="********"
            />
          </div>
          <select
            className="form-select"
            aria-label="Default select example"
            value={role}
            onChange={(e) => setRole(e.target.value)}
          >
            <option defaultValue={true}>Choose your role</option>
            <option value="1">Customer</option>
            <option value="2">Admin</option>
          </select>
          <button className="btn btn-success mt-2">Submit</button>
        </div>
      </div>
    </div>
  );
};

export default ServicePage;