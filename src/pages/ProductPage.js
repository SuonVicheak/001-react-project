import React from "react";
import CounterFuncComponent from "../components/CounterFuncComponent";

const ProductPage = () => {
  return (
    <div className="container">
      <h1>This is the product page</h1>
      <p>
        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Vel incidunt
        dignissimos expedita fugiat repudiandae, molestias blanditiis debitis,
        autem nisi maiores perferendis, harum ut voluptatum ipsum officiis
        quisquam asperiores vitae eum!
      </p>

      <CounterFuncComponent />
    </div>
  );
};

export default ProductPage;
