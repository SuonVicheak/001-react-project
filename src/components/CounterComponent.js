//rafce
//rce = react class export component

import React, { Component } from "react";

//functional component -> stateless component means no state
//class component -> statefull component mean having state
//state is an object that is privately maintained inside a component
export class CounterComponent extends Component {
  constructor(props) {
    super(props);
    //this is where we use normal variables
    //this.counter = 0;

    //this is where we use state
    this.state = {
      counter: 0,
    };
  }

  render() {
    const onIncrease = () => {
      //this.counter++;
      //console.log(this.counter);
      //alert("Increase is clicked!");

      this.setState({
        counter: this.state.counter + 1,
      });
    };
    const onDecrease = () => {
      //this.counter--;
      //console.log(this.counter);
      //alert("Decrease is clicked!");

      this.setState({
        counter: this.state.counter - 1,
      });
    };

    return (
      <div className="text-center">
        <h4>Counter from class component</h4>
        <h1>{this.state.counter}</h1>
        <div>
          <button className="btn btn-success mx-1" onClick={onIncrease}>
            Increase
          </button>
          <button className="btn btn-warning" onClick={onDecrease}>
            Decrease
          </button>
        </div>
      </div>
    );
  }
}

export default CounterComponent;
