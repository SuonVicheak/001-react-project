import React from "react";

const UserGreeting = () => {
  return (
    <div>
      <h3>Welcome back! Mr. User</h3>
      <p>Since you are a user, you have to listen to an admin!</p>
    </div>
  );
};

export default UserGreeting;
