import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";

function ProfileCard({ user }) {
  const { name, email, creationAt, avatar, role } = user;
  return (
    <Card>
      <Card.Img variant="top" src={avatar} />
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Text>
          {email}
          <br />
          <span>Created at : {creationAt}</span>
        </Card.Text>
        <Button variant="primary">{role}</Button>
      </Card.Body>
    </Card>
  );
}

export default ProfileCard;