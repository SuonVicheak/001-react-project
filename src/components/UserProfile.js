import React from "react";
import { Button, Card } from "react-bootstrap";

//object destructuring (props destructure)
function UserProfile({ userData, buttonMessage }) {
  //destruct one time
  const { username, profileImgUrl, bio, gender } = userData;
  return (
    <Card style={{ width: "18rem" }}>
      <Card.Img variant="top" src={profileImgUrl} />
      <Card.Body>
        <Card.Title>{username}</Card.Title>
        <Card.Text>
          <span>Gender : {gender}</span>
          <br />
          {bio}
        </Card.Text>
        <Button variant="primary">{buttonMessage}</Button>
      </Card.Body>
    </Card>
  );
}

export default UserProfile;
