//shortcut : rafce, rfce
//rfce = react functional component with export
//rafce = react arrow functional component with export

import React from "react";
//external stylesheet
import "../styles/UserProfile.css";

//this is the UserCard used with normal case
export const UserCard = (props) => {
  //object stylesheet
  let headerStyle = {
    color: "white",
    backgroundColor: "black",
    padding: "5px",
  };

  return (
    <div
      className="userCard"
      //inline stylesheet
      style={{
        backgroundColor: "#ACE2E1",
        padding: "5px 10px",
        margin: "20px auto",
        borderRadius: "10px",
      }}
    >
      <h1 style={headerStyle}>{props.username}'s profile card</h1>
      <p>Gender: {props.gender}</p>
      <p>
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Delectus
        repudiandae recusandae natus officiis iusto. Vel, aperiam at tempore
        architecto eum veniam consectetur consequuntur, recusandae, officia unde
        ipsum corrupti veritatis illo!
      </p>
      <button className="btn btn-warning">Expore</button>
    </div>
  );
};

//export default UserCard
