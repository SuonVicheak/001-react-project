import React from "react";
import AdminGreeting from "./AdminGreeting";
import UserGreeting from "./UserGreeting";

const Greeting = ({ isAdmin }) => {
  //   if (isAdmin) return <AdminGreeting />;
  //   else <UserGreeting />;

  return isAdmin ? <AdminGreeting /> : <UserGreeting />;
};

export default Greeting;
