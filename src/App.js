//import { UserCard } from "./components/UserCard";
import "bootstrap/dist/css/bootstrap.min.css";
//import UserProfile from "./components/UserProfile";
//import Task from "./components/Task";
//import AdminGreeting from "./components/AdminGreeting";
//import UserGreeting from "./components/UserGreeting";
//import Greeting from "./components/Greeting";
import AppNavBar from "./components/AppNavBar";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./pages/HomePage";
import ProductPage from "./pages/ProductPage";
import NotFoundPage from "./pages/NotFoundPage";
import ServicePage from "./pages/ServicePage";
import UsersPage from "./pages/UsersPage";

//you can use one way data binding like prop
// function AppfirstVersion() {
//   //let username = "James bond";

//   let bonaInfo = {
//     username: "Bona Chan",
//     gender: "male",
//     profileImgUrl:
//       "https://static.vecteezy.com/system/resources/previews/012/941/847/original/illustration-of-avatar-girl-nice-smiling-woman-with-black-hair-flat-icon-on-purple-background-vector.jpg",
//     bio: "Study hard, and enjoy the process of learning!",
//   };

//   //5 tasks
//   let allTasks = [
//     {
//       taskName: "First tasks",
//       description: "This is task description",
//     },
//     {
//       taskName: "Second tasks",
//       description: "This is task description",
//     },
//     {
//       taskName: "Third tasks",
//       description: "This is task description",
//     },
//     {
//       taskName: "Final tasks",
//       description: "This is task description",
//     },
//     {
//       taskName: "Latest tasks",
//       description: "This is task description",
//     },
//   ];

//   allTasks = [];

//   let isAdmin = true;

//   return (
//     //this is the empty fragment
//     <>
//       {/* testing on the conditional rendering */}

//       <div className="text-center mt-3">
//         <Greeting isAdmin={isAdmin} />
//         {/* {isAdmin ? <AdminGreeting /> : <UserGreeting />} */}
//       </div>

//       <div className="container mt-4 d-flex gap-5">
//         <div>
//           <h1>User Info</h1>
//           <UserProfile userData={bonaInfo} buttonMessage="Expore More" />
//         </div>

//         <div className="w-75">
//           <h1>All Tasks</h1>
//           {/* {allTasks.length == 0 ? (
//             <div className="no-item-found text-center">
//               <img
//                 className="img-fluid w-25"
//                 src="https://img.freepik.com/free-vector/location-based-advertisement-geolocation-software-online-gps-app-navigation-system-geographic-restriction-man-searching-address-with-magnifier-vector-isolated-concept-metaphor-illustration_335657-2714.jpg"
//                 alt=""
//               />
//               <h4 className="text-center">There is no tasks...!</h4>
//             </div>
//           ) : (
//             <></>
//           )} */}
//           {allTasks.length === 0 && (
//             <div className="no-item-found text-center">
//               <img
//                 className="img-fluid w-25"
//                 src="https://img.freepik.com/free-vector/location-based-advertisement-geolocation-software-online-gps-app-navigation-system-geographic-restriction-man-searching-address-with-magnifier-vector-isolated-concept-metaphor-illustration_335657-2714.jpg"
//                 alt=""
//               />
//               <h4 className="text-center">There is no tasks...!</h4>
//             </div>
//           )}
//           {/* When loop through list, there must be used with key to specify unique component */}
//           {allTasks.map((task, index) => (
//             <Task key={index} taskData={task} />
//           ))}
//         </div>

//         {/* passing value to UserCard component using props */}
//         {/* <UserCard username="james bond" gender="male" />
//         <UserCard username="bona" gender="female" />
//         <UserCard username="spider man" gender="male" />
//         <UserCard username="cat man" gender="female" /> */}
//       </div>
//     </>
//   );
// }

function App() {
  return (
    <>
      <BrowserRouter>
        <AppNavBar />
        <Routes>
          <Route index path="/" element={<HomePage />} />
          <Route path="/product" element={<ProductPage />} />
          <Route path="/service" element={<ServicePage />} />
          <Route path="/users" element={<UsersPage />} />
          <Route path="*" element={<NotFoundPage />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;

//1. Functional component -> stateless component
//2. Class component -> stateful component
